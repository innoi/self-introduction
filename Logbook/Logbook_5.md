|Logbook 06: Week 6| Date: 26 November 2021|
|------|------|



<div align="center">
_________________________________________________________________________
</div>


|Agenda And Goals|
|------------|

- Doing the survey on the company that also sells the Halfmoon tent.
Ask them about the price of the tent, transportation and installation.

- Getting the quotation for the Halfmoon tent from the possible sellers. 

|Decision in Solving The Problem|
|------------|

- Called and text the companies to ask about the tent and ask for the quotation

- Discussed this matter with Fikri about the payment.

|Method To Solve The Problem|
|------------|

- We did this decision as we need to compare the price on which has a low price.

|Justification in Solving The Problem|
|------------|

- Not to rush in making the buying process.

- Do the best decision as the tent can be used for a long time.

|The Impact of The Decision|
|------------|

- Have a better buying price for the sellers
- Can save the budget

|The Next Step|
|------------|

- Getting the quotation for the seller and analysing which seller has a better price with the same quality and services.

