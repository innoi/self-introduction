# MY ENGINEERING LOGBOOK

|Logbook 07: Week 7| Date: 3 December 2021|
|------|------|

<div align="center">
_________________________________________________________________________
</div>


|Agenda And Goals|
|------------|

- Make sure that the deposit payment for the tent is done.

|Decision in Solving The Problem|
|------------|

- Always keep up to date on whether Fikri and Dr Salah on the deposit payment

|Method Solving The Problem|
|------------|

- By asking Fikri on the progress via WhatsApp.

|Justification in Solving The Problem|
|------------|

- To make sure that the purchase order can be done within this week.

|The Impact of The Decision|
|------------|

- When we do the deposit, the supplier can proceed with the fabrication process of the tent.

|The Next Step|
|------------|

- Update to the supplier about the payment
- Make sure that the tent is going to arrive by the time that we do the assemblymen of the air hybrid.
