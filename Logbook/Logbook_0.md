# MY ENGINEERING LOGBOOK

|Logbook 01: Week 1| Date: 22 October 2021|
|------|------|



<div align="center">
_________________________________________________________________________
</div>


|Agenda And Goals|
|------------|

- Explanation on the project

|Decision in Solving The Problem|
|------------|

- Made the group WhatsApp for the respective subsystems and teams.

|Method To Solve The Problem|
|------------|

- By the discussion on Discord

|Justification in Solving The Problem|
|------------|

- To make it easier to communicate and discuss.

|The Impact of The Decision|
|------------|

- We are able to update the tasks efficiently and faster.

|The Next Step|
|------------|

- Do the task that had been asked by Dr. Salah
