# MY ENGINEERING LOGBOOK

|Logbook: Week 2| Date: 12 November 2021|
|------|------|


<div align="center">
_________________________________________________________________________
</div>


|Agenda And Goals|
|------------|

- In the agenda of this week, we are going to

    1. Present the progress of our subsystem to another subsystem.

    2. Distribute the roles in the teams which are checker, process monitor, recorder and coordinator. 

    3. Make sure that every team member knows how to do the engineering logbook on Gitlab and submit their logbook on the IDP    Project file.


|Decision in Solving The Problem|
|------------|


- To solve the problem, we did the meeting and asked them personally about the problem that they were facing. Then, we tried to solve the problems together with other team members by asking them to share their screen on Discord.


|Method To Solve The Problem|
|------------|


- We did this decision as a team, we did the brainstorm session and tried to solve the problem together. 


|Justification in Solving The Problem|
|------------|


- We did this because we did not want any of our team members to leave behind in doing the IDP. Thus, we were concerned about other team members’ work in order to make sure that they did their tasks. 


|The Impact of The Decision|
|------------|

- By doing this, I ensure that no one in our team members leaves behind and they will responsible for doing their tasks.


|The Next Step|
|------------|

- As for the next step, we are going to continue doing the tasks under the subsystem given and update to the team so that everyone knows about the whole progress of IDP.

